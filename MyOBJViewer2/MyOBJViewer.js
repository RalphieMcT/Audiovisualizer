// MyOBJViewer.js, based on OBJViewer by matsuda and itami
//
// Time-stamp: "2015-01-26 09:27:20 gas@offsim.no (Geir Atle Storhaug)"
//
// Vertex shader program for objects with one sigle color
var VSHADER_SOURCE_SINGLE_COLOR =
  'attribute vec4 a_Position;\n' +
  'attribute vec4 a_Normal;\n' +
  'uniform mat4 u_mvpMatrix;\n' +
  'uniform mat4 u_NormalMatrix;\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_Position = u_mvpMatrix * a_Position;\n' +
  '  vec3 lightDirection = normalize(vec3(0.0, 0.5, 0.7));\n' + // Light direction
  '  vec4 color = vec4(1.0, 0.4, 0.0, 1.0);\n' +  // Object color
  '  vec3 normal = normalize((u_NormalMatrix * a_Normal).xyz);\n' +
  '  float nDotL = max(dot(normal, lightDirection), 0.0);\n' +
  '  v_Color = vec4(color.rgb * nDotL + vec3(0.1), color.a);\n' +
  '}\n';

// Fragment shader program for objects with one single color
var FSHADER_SOURCE_SINGLE_COLOR =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color;\n' +
  '}\n';

// Vertex shader program for objects with color per vertex
var VSHADER_SOURCE_MULTI_COLOR =
  'attribute vec4 a_Position;\n' +
  'attribute vec4 a_Color;\n' +
  'attribute vec4 a_Normal;\n' +
  'uniform mat4 u_mvpMatrix;\n' +
  'uniform mat4 u_NormalMatrix;\n' +
  'varying vec4 v_Color;\n' +
  'varying vec3 v_Normal;\n' +
  'void main() {\n' +
  '  gl_Position = u_mvpMatrix * a_Position;\n' +
  '  v_Color = a_Color;\n' +
  '  v_Normal = vec3(u_NormalMatrix * a_Normal);\n' +
  '}\n';

// Fragment shader program for objects with color per vertex
var FSHADER_SOURCE_MULTI_COLOR =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'uniform vec3 u_AmbientLight;\n' +   // Ambient light color
  'varying vec4 v_Color;\n' +
  'varying vec3 v_Normal;\n' +
  'void main() {\n' +
     // Normalize the normal because it is interpolated and not 1.0 in length any more
  '  vec3 normal = normalize(v_Normal);\n' +
  '  vec3 lightDirection = normalize(vec3(0.0, 0.5, 0.7));\n' + // Light direction
  '  vec3 lightColor = vec3(0.99, 0.99, 0.99);\n' + // Light color
     // The dot product of the light direction and the normal
  '  float nDotL = max(dot(lightDirection, normal), 0.0);\n' +
     // Calculate the final color from diffuse reflection and ambient reflection
  '  vec3 diffuse = lightColor * v_Color.rgb * nDotL;\n' +
  '  vec3 ambient = u_AmbientLight * v_Color.rgb;\n' +
  '  gl_FragColor = vec4(diffuse + ambient, v_Color.a);\n' +
  '}\n';

// Global variables that need to maintain their value after main() has executed:
var g_myG = {
};
var g_camera = {
    azimuth: -20.0,
    elevation: 30.0,
    distance: 20.0
};


function main() {
    // Retrieve <canvas> element
    g_myG.canvas = document.getElementById('webgl');
    g_myG.outPara = document.getElementById('outPara');

    // Get the rendering context for WebGL
    var gl = getWebGLContext(g_myG.canvas);
    if (!gl) {
        console.log('Failed to get the rendering context for WebGL');
        return;
    }

    // Initialize shaders
    g_myG.scProg = createProgram(gl, VSHADER_SOURCE_SINGLE_COLOR, FSHADER_SOURCE_SINGLE_COLOR);
    g_myG.mcProg = createProgram(gl, VSHADER_SOURCE_MULTI_COLOR, FSHADER_SOURCE_MULTI_COLOR);
    if (!g_myG.scProg || !g_myG.mcProg) {
        console.log('Failed to intialize shaders.');
        return;
    }

    // Set the clear color and enable the depth test
    gl.clearColor(0.2, 0.3, 0.4, 1.0);
    gl.enable(gl.DEPTH_TEST);

    // Get the storage locations of attribute and uniform variables
    g_myG.gl = gl;
    g_myG.scProg.a_Position = gl.getAttribLocation(g_myG.scProg, 'a_Position');
    g_myG.scProg.a_Normal = gl.getAttribLocation(g_myG.scProg, 'a_Normal');
    g_myG.scProg.u_mvpMatrix = gl.getUniformLocation(g_myG.scProg, 'u_mvpMatrix');
    g_myG.scProg.u_NormalMatrix = gl.getUniformLocation(g_myG.scProg, 'u_NormalMatrix');
    g_myG.mcProg.a_Position = gl.getAttribLocation(g_myG.mcProg, 'a_Position');
    g_myG.mcProg.a_Normal = gl.getAttribLocation(g_myG.mcProg, 'a_Normal');
    g_myG.mcProg.a_Color = gl.getAttribLocation(g_myG.mcProg, 'a_Color');
    g_myG.mcProg.u_AmbientLight = gl.getUniformLocation(g_myG.mcProg, 'u_AmbientLight');
    g_myG.mcProg.u_mvpMatrix = gl.getUniformLocation(g_myG.mcProg, 'u_mvpMatrix');
    g_myG.mcProg.u_NormalMatrix = gl.getUniformLocation(g_myG.mcProg, 'u_NormalMatrix');

    if (g_myG.scProg.a_Position < 0 || g_myG.scProg.a_Normal < 0 ||
        !g_myG.scProg.u_NormalMatrix || !g_myG.scProg.u_mvpMatrix ||
        g_myG.mcProg.a_Position < 0 || g_myG.mcProg.a_Normal < 0 || g_myG.mcProg.a_Color < 0 ||
        !g_myG.mcProg.u_mvpMatrix || !g_myG.mcProg.u_NormalMatrix) {
        console.log('One or more attribute or uniform variables not found');
        return;
    }

    // Set the vertex information for the Cube
    g_myG.scProg.cubeModel = initCubeVertexBuffers(gl, g_myG.scProg);
    if (!g_myG.scProg.cubeModel) {
        console.log('Failed to set the vertex information for the Cube');
        return;
    }

    gl.useProgram(g_myG.mcProg);

    gl.uniform3f(g_myG.mcProg.u_AmbientLight, 0.2, 0.2, 0.2);

    // Prepare empty buffer objects for vertex coordinates, colors, and normals for the OBJ Object
    g_myG.mcProg.objModel = initOBJVertexBuffers(gl, g_myG.mcProg);
    if (!g_myG.mcProg.objModel) {
        console.log('Failed to set the vertex information for the OBJ object');
        return;
    }
    g_myG.mcProg.drawingInfo = null; // The information for drawing the OBJ 3D model

    initEventHandlers(g_myG.canvas);

    // Specify projection
    g_myG.projMatrix = new Matrix4();
    g_myG.projMatrix.setPerspective(30.0, g_myG.canvas.width/g_myG.canvas.height, 1.0, 500.0);

    // Start reading the OBJ file
    readOBJFile('monkey.obj', gl, g_myG.objModel, 1, true);
  //  readOBJFile('cube.obj', gl, g_myG.objModel, 1, true);
    draw();
}


function initCubeVertexBuffers(gl, program) {
    // Coordinates（Cube which length of one side is 1 with the origin on the center of the bottom)
    var vertices = new Float32Array([
        0.5, 0.5, 0.5, -0.5, 0.5, 0.5, -0.5,-0.5, 0.5,  0.5,-0.5, 0.5, // v0-v1-v2-v3 front
        0.5, 0.5, 0.5,  0.5,-0.5, 0.5,  0.5,-0.5,-0.5,  0.5, 0.5,-0.5, // v0-v3-v4-v5 right
        0.5, 0.5, 0.5,  0.5, 0.5,-0.5, -0.5, 0.5,-0.5, -0.5, 0.5, 0.5, // v0-v5-v6-v1 up
       -0.5, 0.5, 0.5, -0.5, 0.5,-0.5, -0.5,-0.5,-0.5, -0.5,-0.5, 0.5, // v1-v6-v7-v2 left
       -0.5,-0.5,-0.5,  0.5,-0.5,-0.5,  0.5,-0.5, 0.5, -0.5,-0.5, 0.5, // v7-v4-v3-v2 down
        0.5,-0.5,-0.5, -0.5,-0.5,-0.5, -0.5, 0.5,-0.5,  0.5, 0.5,-0.5  // v4-v7-v6-v5 back
    ]);

    // Normal
    var normals = new Float32Array([
        0.0, 0.0, 1.0,  0.0, 0.0, 1.0,  0.0, 0.0, 1.0,  0.0, 0.0, 1.0, // v0-v1-v2-v3 front
        1.0, 0.0, 0.0,  1.0, 0.0, 0.0,  1.0, 0.0, 0.0,  1.0, 0.0, 0.0, // v0-v3-v4-v5 right
        0.0, 1.0, 0.0,  0.0, 1.0, 0.0,  0.0, 1.0, 0.0,  0.0, 1.0, 0.0, // v0-v5-v6-v1 up
       -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, // v1-v6-v7-v2 left
        0.0,-1.0, 0.0,  0.0,-1.0, 0.0,  0.0,-1.0, 0.0,  0.0,-1.0, 0.0, // v7-v4-v3-v2 down
        0.0, 0.0,-1.0,  0.0, 0.0,-1.0,  0.0, 0.0,-1.0,  0.0, 0.0,-1.0  // v4-v7-v6-v5 back
    ]);

    // Indices of the vertices
    var indices = new Uint8Array([
        0, 1, 2,   0, 2, 3,    // front
        4, 5, 6,   4, 6, 7,    // right
        8, 9,10,   8,10,11,    // up
        12,13,14,  12,14,15,    // left
        16,17,18,  16,18,19,    // down
        20,21,22,  20,22,23     // back
    ]);

    var o = new Object(); // Utilize Object object to return multiple buffer objects
    // Write the vertex property to buffers (coordinates and normals)
    o.vertexBuffer = initCubeArrayBuffer(gl, vertices, program.a_Position, 3, gl.FLOAT);
    o.normalBuffer = initCubeArrayBuffer(gl, normals, program.a_Normal, 3, gl.FLOAT);

    o.indexBuffer = gl.createBuffer();

    if (!o.vertexBuffer || !o.normalBuffer || !o.indexBuffer) {
        console.log('Failed to create one or more buffer object for the cube');
        return null;
    }
    // Write the indices to the buffer object
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, o.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

    o.numIndices = indices.length;

    return o;
}


function initCubeArrayBuffer(gl, data, a_attribute, num, type) {
    // Create a buffer object
    var buffer = gl.createBuffer();
    if (!buffer) {
        console.log('Failed to create the buffer object for the cube');
        return false;
    }
    // Write date into the buffer object
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

    // Keep the information necessary to assign to the attribute variable later
    buffer.num = num;
    buffer.type = type;

    return buffer;
}


// Create an buffer object and perform an initial configuration
function initOBJVertexBuffers(gl, program) {
    var o = new Object(); // Utilize Object object to return multiple buffer objects
    o.vertexBuffer = createEmptyArrayBuffer(gl, program.a_Position, 3, gl.FLOAT);
    o.normalBuffer = createEmptyArrayBuffer(gl, program.a_Normal, 3, gl.FLOAT);
    o.colorBuffer = createEmptyArrayBuffer(gl, program.a_Color, 4, gl.FLOAT);
    o.indexBuffer = gl.createBuffer();
    if (!o.vertexBuffer || !o.normalBuffer || !o.colorBuffer || !o.indexBuffer) { return null; }

    return o;
}


// Create a buffer object, assign it to attribute variables, and enable the assignment
function createEmptyArrayBuffer(gl, a_attribute, num, type) {
    var buffer =  gl.createBuffer();  // Create a buffer object
    if (!buffer) {
        console.log('Failed to create the buffer object for the OBJ object');
        return null;
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

    // Keep the information necessary to assign to the attribute variable later
    buffer.num = num;
    buffer.type = type;

    return buffer;
}


// Read a file
function readOBJFile(fileName, gl, model, scale, reverse) {
    console.log("readOBJFile");
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (request.readyState === 4 && request.status !== 404) {
            onReadOBJFile(request.responseText, fileName, gl, model, scale, reverse);
        }
    }
    request.open('GET', fileName, true); // Create a request to acquire the file
    request.send();                      // Send the request
}

var g_objDoc = null;      // The information of OBJ file


// OBJ File has been read
function onReadOBJFile(fileString, fileName, gl, o, scale, reverse) {
    console.log("onReadOBJFile");
    var objDoc = new OBJDoc(fileName);  // Create a OBJDoc object
    var result = objDoc.parse(fileString, scale, reverse); // Parse the file
    if (!result) {
        g_objDoc = null; g_myG.mcProg.drawingInfo = null;
        console.log("OBJ file parsing error.");
        return;
    }
    g_objDoc = objDoc;
    draw();
}


// Note: These variables need to be global, since then need to
// maintain their value between invocations of the event handlers.
var g_dragging = false;           // Dragging or not
var g_lastX = -1, g_lastY = -1;   // Last position of the mouse

function initEventHandlers(canvas) {
    canvas.onmousedown = function(ev) {   // Mouse is pressed
        var x = ev.clientX, y = ev.clientY;
        // Start dragging if mouse is in <canvas>
        var rect = ev.target.getBoundingClientRect();
        if (rect.left <= x && x < rect.right && rect.top <= y && y < rect.bottom) {
            g_lastX = x; g_lastY = y;
            g_dragging = true;
        }
    };

    canvas.onmouseup = function(ev) { g_dragging = false;  }; // Mouse is released

    canvas.onmousemove = function(ev) { // Mouse is moved
        var x = ev.clientX, y = ev.clientY;
        if (g_dragging) {
            var factor = 100/canvas.height; // The rotation ratio
            var dx = factor * (x - g_lastX);
            var dy = factor * (y - g_lastY);
            // Limit x-axis rotation angle to -90 to 90 degrees
            g_camera.elevation = Math.max(Math.min(g_camera.elevation + dy, 90.0), -90.0);
            // Limit y-axis rotation angle to 0 to 360 degrees
            g_camera.azimuth = (g_camera.azimuth + dx + 360.0) % 360.0;

            draw();
        }
        g_lastX = x, g_lastY = y;
    };

    canvas.onmousewheel = function(ev) { // Mouse wheel has moved
        var dm = ev.deltaY * 0.01;
        g_camera.distance = Math.max(2.0, g_camera.distance + dm);

        draw();
    };
}


// Coordinate transformation matrix
var g_mvpMatrix = new Matrix4();
var g_normalMatrix = new Matrix4();


// Draw everything in the scene
function draw() {
    var gl = g_myG.gl;

    console.log("draw, (g_objDoc != null && g_objDoc.isMTLComplete()):" + (g_objDoc != null && g_objDoc.isMTLComplete()));
    if (g_objDoc != null && g_objDoc.isMTLComplete()){ // OBJ and all MTLs are available
        g_myG.mcProg.drawingInfo = onReadComplete(gl, g_myG.mcProg.objModel, g_objDoc);
        g_objDoc = null;
    }
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);  // Clear color and depth buffers

    // Display variables:
    g_myG.outPara.innerHTML = "azimuth: " + Math.round(g_camera.azimuth) +
        ", elevation: " + Math.round(g_camera.elevation) +
        ", distance: " + Math.round(g_camera.distance * 10.0) / 10.0;

    var viewMatrix = new Matrix4();
    viewMatrix.setTranslate(0.0, 0.0, -g_camera.distance);
    viewMatrix.rotate(g_camera.elevation, 1.0, 0.0, 0.0); // Rotation around x-axis
    viewMatrix.rotate(g_camera.azimuth, 0.0, 1.0, 0.0); // Rotation around y-axis

    drawAxes(gl, g_myG.scProg, viewMatrix);

    if (g_myG.mcProg.drawingInfo) {  // If OBJ and MTL files are loaded
        var objModelMatrix = new Matrix4();
        for (i = 1; i < 10; i += 1) {
            objModelMatrix.setTranslate(i, 5, 4);
            drawOBJ(gl, g_myG.mcProg, viewMatrix, objModelMatrix);
        }
    }
}


function drawAxes(gl, p, vM) {
    var vpM = new Matrix4();
    var mM = new Matrix4();

    vpM.set(g_myG.projMatrix);
    vpM.multiply(vM);

    gl.useProgram(p);

    initAttributeVariable(gl, p.a_Position, p.cubeModel.vertexBuffer);
    initAttributeVariable(gl, p.a_Normal, p.cubeModel.normalBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p.cubeModel.indexBuffer);
    numI = g_myG.scProg.cubeModel.numIndices;

    // Draw origin
    mM.setTranslate(0.0, 0.0, 0.0);
    drawBox(gl, numI, 0.2, 0.2, 0.2, vpM, mM, p.u_mvpMatrix, p.u_NormalMatrix);

    // Draw X-axis
    mM.setTranslate(5.0, 0.0, 0.0);
    drawBox(gl, numI, 10.0, 0.1, 0.1, vpM, mM, p.u_mvpMatrix, p.u_NormalMatrix);

    // Draw Y-axis
    mM.setTranslate(0.0, 5.0, 0.0);
    drawBox(gl, numI, 0.1, 10.0, 0.1, vpM, mM, p.u_mvpMatrix, p.u_NormalMatrix);

    // Draw Z-axis
    mM.setTranslate(0.0, 0.0, 5.0);
    drawBox(gl, numI, 0.1, 0.1, 10.0, vpM, mM, p.u_mvpMatrix, p.u_NormalMatrix);
}


// Draw rectangular solid
function drawBox(gl, numI, width, height, depth, vpM, mM, u_mvpMatrix, u_NormalMatrix) {
    var tmpMM = new Matrix4();

    tmpMM.set(mM);
    // Scale a cube and draw
    tmpMM.scale(width, height, depth);
    // Calculate the model view project matrix and pass it to u_mvpMatrix
    g_mvpMatrix.set(vpM);
    g_mvpMatrix.multiply(tmpMM);
    gl.uniformMatrix4fv(u_mvpMatrix, false, g_mvpMatrix.elements);
    // Calculate the normal transformation matrix and pass it to u_NormalMatrix
    g_normalMatrix.setInverseOf(tmpMM);
    g_normalMatrix.transpose();
    gl.uniformMatrix4fv(u_NormalMatrix, false, g_normalMatrix.elements);
    // Draw
    gl.drawElements(gl.TRIANGLES, numI, gl.UNSIGNED_BYTE, 0);
}


function drawOBJ(gl, p, viewM, modelM) {
    gl.useProgram(p);

    initAttributeVariable(gl, p.a_Position, p.objModel.vertexBuffer);
    initAttributeVariable(gl, p.a_Normal, p.objModel.normalBuffer);
    initAttributeVariable(gl, p.a_Color, p.objModel.colorBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p.objModel.indexBuffer);

    // Calculate the normal transformation matrix and pass it to u_NormalMatrix
    g_normalMatrix.setInverseOf(modelM);
    g_normalMatrix.transpose();
    gl.uniformMatrix4fv(p.u_NormalMatrix, false, g_normalMatrix.elements);

    // Calculate the model view project matrix and pass it to u_mvpMatrix
    g_mvpMatrix.set(g_myG.projMatrix);
    g_mvpMatrix.multiply(viewM);
    g_mvpMatrix.multiply(modelM);
    gl.uniformMatrix4fv(p.u_mvpMatrix, false, g_mvpMatrix.elements);

    // Draw
    gl.drawElements(gl.TRIANGLES, p.drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);
}


// Assign the buffer objects and enable the assignment
function initAttributeVariable(gl, a_attribute, buffer) {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(a_attribute, buffer.num, buffer.type, false, 0, 0);
    gl.enableVertexAttribArray(a_attribute);
}


// OBJ File has been read completely
function onReadComplete(gl, model, objDoc) {
    console.log("onReadComplete");
    // Acquire the vertex coordinates and colors from OBJ file
    var drawingInfo = objDoc.getDrawingInfo();

    // Write date into the buffer object
    gl.bindBuffer(gl.ARRAY_BUFFER, model.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.vertices, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, model.normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.normals, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, model.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.colors, gl.STATIC_DRAW);

    // Write the indices to the buffer object
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, drawingInfo.indices, gl.STATIC_DRAW);

    return drawingInfo;
}


var g_matrixStack = []; // Array for storing a matrix
function pushMatrix(m) { // Store the specified matrix to the array
    var m2 = new Matrix4(m);
    g_matrixStack.push(m2);
}


function popMatrix() { // Retrieve the matrix from the array
    return g_matrixStack.pop();
}
