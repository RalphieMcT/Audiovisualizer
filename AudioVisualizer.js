// Vertex shader program for objects with color per vertex
var VSHADER_SOURCE_MULTI_COLOR =
  'attribute vec4 a_Position;\n' +
  'attribute vec4 a_Color;\n' +
  'attribute vec4 a_Normal;\n' +
  'uniform mat4 u_mvpMatrix;\n' +
  'uniform mat4 u_NormalMatrix;\n' +
  'varying vec4 v_Color;\n' +
  'varying vec3 v_Normal;\n' +
  'void main() {\n' +
  '  gl_Position = u_mvpMatrix * a_Position;\n' +
  '  v_Color = a_Color;\n' +
  '  v_Normal = vec3(u_NormalMatrix * a_Normal);\n' +
  '}\n';

// Fragment shader program for objects with color per vertex
var FSHADER_SOURCE_MULTI_COLOR =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'uniform vec3 u_AmbientLight;\n' +   // Ambient light color
  'varying vec4 v_Color;\n' +
  'varying vec3 v_Normal;\n' +
  'void main() {\n' +
     // Normalize the normal because it is interpolated and not 1.0 in length any more
  '  vec3 normal = normalize(v_Normal);\n' +
  '  vec3 lightDirection = normalize(vec3(0.0, 0.5, 0.7));\n' + // Light direction
  '  vec3 lightColor = vec3(0.99, 0.99, 0.99);\n' + // Light color
     // The dot product of the light direction and the normal
  '  float nDotL = max(dot(lightDirection, normal), 0.0);\n' +
     // Calculate the final color from diffuse reflection and ambient reflection
  '  vec3 diffuse = lightColor * v_Color.rgb * nDotL;\n' +
  '  vec3 ambient = u_AmbientLight * v_Color.rgb;\n' +
  '  gl_FragColor = vec4(diffuse + ambient, v_Color.a);\n' +
  '}\n';

g_audioContext = new AudioContext()
sourceNode = 0
analyser = 0
javascriptNode = 0
url = "01 - The Easton Ellises - Liquorstore.mp3"
var g_myG = {
}
var g_camera = {
    azimuth: -20.0,
    elevation: 30.0,
    distance: 20.0
}
function main(){
  g_myG.canvas = document.getElementById("webgl")
  var gl = getWebGLContext(g_myG.canvas)
  if(!gl) {
    alert("no GL context found")
  }
  g_myG.mcProg = createProgram(gl, VSHADER_SOURCE_MULTI_COLOR, FSHADER_SOURCE_MULTI_COLOR);
  if (!g_myG.mcProg) {
      console.log('Failed to intialize shaders.')
      return
  }
  gl.clearColor(0.2, 0.3, 0.4, 1.0)
  gl.enable(gl.DEPTH_TEST)

  g_myG.gl = gl
  g_myG.mcProg.a_Position = gl.getAttribLocation(g_myG.mcProg, 'a_Position')
  g_myG.mcProg.a_Normal = gl.getAttribLocation(g_myG.mcProg, 'a_Normal')
  g_myG.mcProg.a_Color = gl.getAttribLocation(g_myG.mcProg, 'a_Color')
  g_myG.mcProg.u_AmbientLight = gl.getUniformLocation(g_myG.mcProg, 'u_AmbientLight')
  g_myG.mcProg.u_mvpMatrix = gl.getUniformLocation(g_myG.mcProg, 'u_mvpMatrix')
  g_myG.mcProg.u_NormalMatrix = gl.getUniformLocation(g_myG.mcProg, 'u_NormalMatrix')

  if (g_myG.mcProg.a_Position < 0 || g_myG.mcProg.a_Normal < 0 || g_myG.mcProg.a_Color < 0 ||
      !g_myG.mcProg.u_mvpMatrix || !g_myG.mcProg.u_NormalMatrix) {
      console.log('One or more attribute or uniform variables not found')
      return;
  }

  gl.useProgram(g_myG.mcProg);
  gl.uniform3f(g_myG.mcProg.u_AmbientLight, 0.2, 0.2, 0.2)

  g_myG.mcProg.objModel = initOBJVertexBuffers(gl, g_myG.mcProg, 'monkey.obj')
  g_myG.mcProg.cubeModel = initOBJVertexBuffers(gl, g_myG.mcProg, 'cube.obj')

  if(!g_myG.mcProg.objModel){
    console.log("Failed to set the vertex information for the OBJ object")
  }
  if(!g_myG.mcProg.cubeModel){
    console.log("Failed to set the vertex information for the Cube object")
  }
  g_myG.mcProg.objModel.drawingInfo = null

  initEventHandlers(g_myG.canvas)
  g_myG.projMatrix = new Matrix4()
  g_myG.projMatrix.setPerspective(30.0, g_myG.canvas.width/g_myG.canvas.height, 1.0, 500.0)

  readOBJFile('monkey.obj', gl, g_myG.mcProg.objModel, 1, true);
  readOBJFile('cube.obj', gl, g_myG.mcProg.cubeModel, 1, true);

  if(!window.AudioContext) {
      alert("Web Audio API is not supported in this browser")
  }
  setupAudioNodes()
  loadSound(url)
}

// Note: These variables need to be global, since then need to
// maintain their value between invocations of the event handlers.
var g_dragging = false;           // Dragging or not
var g_lastX = -1, g_lastY = -1;   // Last position of the mouse
function initEventHandlers(canvas) {
    canvas.onmousedown = function(ev) {   // Mouse is pressed
        var x = ev.clientX, y = ev.clientY;
        // Start dragging if mouse is in <canvas>
        var rect = ev.target.getBoundingClientRect();
        if (rect.left <= x && x < rect.right && rect.top <= y && y < rect.bottom) {
            g_lastX = x; g_lastY = y;
            g_dragging = true;
        }
    };

    canvas.onmouseup = function(ev) { g_dragging = false;  }; // Mouse is released

    canvas.onmousemove = function(ev) { // Mouse is moved
        var x = ev.clientX, y = ev.clientY;
        if (g_dragging) {
            var factor = 100/canvas.height; // The rotation ratio
            var dx = factor * (x - g_lastX);
            var dy = factor * (y - g_lastY);
            // Limit x-axis rotation angle to -90 to 90 degrees
            g_camera.elevation = Math.max(Math.min(g_camera.elevation + dy, 90.0), -90.0);
            // Limit y-axis rotation angle to 0 to 360 degrees
            g_camera.azimuth = (g_camera.azimuth + dx + 360.0) % 360.0;

            draw();
        }
        g_lastX = x, g_lastY = y;
    };

    canvas.onmousewheel = function(ev) { // Mouse wheel has moved
        var dm = ev.deltaY * 0.01;
        g_camera.distance = Math.max(2.0, g_camera.distance + dm);

        draw();
    };
}

// Create an buffer object and perform an initial configuration
function initOBJVertexBuffers(gl, program) {
    var o = new Object(); // Utilize Object object to return multiple buffer objects
    o.vertexBuffer = createEmptyArrayBuffer(gl, program.a_Position, 3, gl.FLOAT);
    o.normalBuffer = createEmptyArrayBuffer(gl, program.a_Normal, 3, gl.FLOAT);
    o.colorBuffer = createEmptyArrayBuffer(gl, program.a_Color, 4, gl.FLOAT);
    o.indexBuffer = gl.createBuffer();
    if (!o.vertexBuffer || !o.normalBuffer || !o.colorBuffer || !o.indexBuffer) { return null; }
    return o;
}
// Coordinate transformation matrix
var g_mvpMatrix = new Matrix4();
var g_normalMatrix = new Matrix4();
;
function draw() {
    var gl = g_myG.gl;

    console.log("draw, (g_objDoc != null && g_objDoc.isMTLComplete()):" + (g_objDoc != null && g_objDoc.isMTLComplete()));
    if (g_objDoc != null && g_objDoc.isMTLComplete()){ // OBJ and all MTLs are available
        g_myG.mcProg.objModel.drawingInfo = onReadComplete(gl, g_myG.mcProg.objModel, g_objDoc);
        g_objDoc = null;
    }

    console.log("draw, (g_cubeDoc != null && g_cubeDoc.isMTLComplete()):" + (g_cubeDoc != null && g_cubeDoc.isMTLComplete()));
    if (g_cubeDoc != null && g_cubeDoc.isMTLComplete()){ // OBJ and all MTLs are available
        g_myG.mcProg.cubeModel.drawingInfo = onReadComplete(gl, g_myG.mcProg.cubeModel, g_cubeDoc);
        g_cubeDoc = null;
    }
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);  // Clear color and depth buffers

    // Display variables:
    //g_myG.outPara.innerHTML = "azimuth: " + Math.round(g_camera.azimuth) +
      //  ", elevation: " + Math.round(g_camera.elevation) +
        //", distance: " + Math.round(g_camera.distance * 10.0) / 10.0;

    var viewMatrix = new Matrix4();
    viewMatrix.setTranslate(0.0, 0.0, -g_camera.distance);
    viewMatrix.rotate(g_camera.elevation, 1.0, 0.0, 0.0); // Rotation around x-axis
    viewMatrix.rotate(g_camera.azimuth, 0.0, 1.0, 0.0); // Rotation around y-axis

    drawAxes(gl, g_myG.mcProg, viewMatrix);

    if (g_myG.mcProg.objModel.drawingInfo) {  // If OBJ and MTL files are loaded
        var objModelMatrix = new Matrix4();
        for (i = 1; i < 10; i += 1) {
            objModelMatrix.setTranslate(i, 5, 4);
            drawOBJ(gl, g_myG.mcProg, viewMatrix, objModelMatrix);
        }
    }
}

function drawAxes(gl, prog, vM) {
    var vpM = new Matrix4();
    var mM = new Matrix4();

    vpM.set(g_myG.projMatrix);
    vpM.multiply(vM);

    //gl.useProgram(prog);

    initAttributeVariable(gl, prog.a_Position, prog.cubeModel.vertexBuffer);
    initAttributeVariable(gl, prog.a_Normal, prog.cubeModel.normalBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, prog.cubeModel.indexBuffer);
    numI = g_myG.mcProg.cubeModel.numIndices;

    // Draw origin
    mM.setTranslate(0.0, 0.0, 0.0);
    drawBox(gl, numI, 0.2, 0.2, 0.2, vpM, mM, prog.u_mvpMatrix, prog.u_NormalMatrix);
  //  drawOBJ(gl, g_myG.mcProg, vpM, g_myG.mcProg.cubeModel)
    // Draw X-axis
    mM.setTranslate(5.0, 0.0, 0.0);
    drawBox(gl, numI, 10.0, 0.1, 0.1, vpM, mM, prog.u_mvpMatrix, prog.u_NormalMatrix);

    // Draw Y-axis
    mM.setTranslate(0.0, 5.0, 0.0);
    drawBox(gl, numI, 0.1, 10.0, 0.1, vpM, mM, prog.u_mvpMatrix, prog.u_NormalMatrix);

    // Draw Z-axis
    mM.setTranslate(0.0, 0.0, 5.0);
    drawBox(gl, numI, 0.1, 0.1, 10.0, vpM, mM, prog.u_mvpMatrix, prog.u_NormalMatrix);
}

// Draw rectangular solid
function drawBox(gl, numI, width, height, depth, vpM, mM, u_mvpMatrix, u_NormalMatrix) {
    var tmpMM = new Matrix4();

    tmpMM.set(mM);
    // Scale a cube and draw
    tmpMM.scale(width, height, depth);
    // Calculate the model view project matrix and pass it to u_mvpMatrix
    g_mvpMatrix.set(vpM);
    g_mvpMatrix.multiply(tmpMM);
    gl.uniformMatrix4fv(u_mvpMatrix, false, g_mvpMatrix.elements);
    // Calculate the normal transformation matrix and pass it to u_NormalMatrix
    g_normalMatrix.setInverseOf(tmpMM);
    g_normalMatrix.transpose();
    gl.uniformMatrix4fv(u_NormalMatrix, false, g_normalMatrix.elements);
    // Draw
    gl.drawElements(gl.TRIANGLES, numI, gl.UNSIGNED_BYTE, 0);
}

function drawOBJ(gl, p, viewM, modelM) {
    gl.useProgram(p);

    initAttributeVariable(gl, p.a_Position, p.objModel.vertexBuffer);
    initAttributeVariable(gl, p.a_Normal, p.objModel.normalBuffer);
    initAttributeVariable(gl, p.a_Color, p.objModel.colorBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p.objModel.indexBuffer);

    // Calculate the normal transformation matrix and pass it to u_NormalMatrix
    g_normalMatrix.setInverseOf(modelM);
    g_normalMatrix.transpose();
    gl.uniformMatrix4fv(p.u_NormalMatrix, false, g_normalMatrix.elements);

    // Calculate the model view project matrix and pass it to u_mvpMatrix
    g_mvpMatrix.set(g_myG.projMatrix);
    g_mvpMatrix.multiply(viewM);
    g_mvpMatrix.multiply(modelM);
    gl.uniformMatrix4fv(p.u_mvpMatrix, false, g_mvpMatrix.elements);

    // Draw
    gl.drawElements(gl.TRIANGLES, p.objModel.drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);
}

// Create a buffer object, assign it to attribute variables, and enable the assignment
function createEmptyArrayBuffer(gl, a_attribute, num, type) {
    var buffer =  gl.createBuffer();  // Create a buffer object
    if (!buffer) {
        console.log('Failed to create the buffer object for the OBJ object');
        return null;
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

    // Keep the information necessary to assign to the attribute variable later
    buffer.num = num;
    buffer.type = type;

    return buffer;
}
// Assign the buffer objects and enable the assignment
function initAttributeVariable(gl, a_attribute, buffer) {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(a_attribute, buffer.num, buffer.type, false, 0, 0);
    gl.enableVertexAttribArray(a_attribute);
}
// Read a file
function readOBJFile(fileName, gl, model, scale, reverse) {
    console.log("readOBJFile");
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (request.readyState === 4 && request.status !== 404) {
            onReadOBJFile(request.responseText, fileName, gl, model, scale, reverse);
        }
    }
    request.open('GET', fileName, true); // Create a request to acquire the file
    request.send();                      // Send the request
}

var g_objDoc = null;      // The information of OBJ file
var g_cubeDoc = null

// OBJ File has been read
function onReadOBJFile(fileString, fileName, gl, o, scale, reverse) {
    console.log("onReadOBJFile");
    var objDoc = new OBJDoc(fileName);  // Create a OBJDoc object
    var result = objDoc.parse(fileString, scale, reverse); // Parse the file
    if (!result) {
        g_objDoc = null; g_myG.mcProg.o.drawingInfo = null;
        console.log("OBJ file parsing error.");
        return;
    }
    g_objDoc = objDoc;
    console.log("object is: "+ o);
    draw();
}

// OBJ File has been read completely
function onReadComplete(gl, model, objDoc) {
    console.log("onReadComplete");
    // Acquire the vertex coordinates and colors from OBJ file
    var drawingInfo = objDoc.getDrawingInfo();

    // Write date into the buffer object
    gl.bindBuffer(gl.ARRAY_BUFFER, model.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.vertices, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, model.normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.normals, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, model.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.colors, gl.STATIC_DRAW);

    // Write the indices to the buffer object
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, drawingInfo.indices, gl.STATIC_DRAW);

    return drawingInfo;
}

var g_matrixStack = []; // Array for storing a matrix
function pushMatrix(m) { // Store the specified matrix to the array
    var m2 = new Matrix4(m);
    g_matrixStack.push(m2);
}

function popMatrix() { // Retrieve the matrix from the array
    return g_matrixStack.pop();
}

function loadSound(url) {
    var request = new XMLHttpRequest()
    request.open('GET', url, true)
    request.responseType = 'arraybuffer'

    // When loaded decode the datas
    request.onload = function() {
        // decode the data
        g_audioContext.decodeAudioData(request.response, function(buffer) {
            // when the audio is decoded play the sound
            playSound(buffer)
        }, onError)

    }
    request.send()
}

function setupAudioNodes(){
  javascriptNode = g_audioContext.createScriptProcessor(0, 1, 1) //for direct audio processing
  javascriptNode.connect(g_audioContext.destination)

  analyser = g_audioContext.createAnalyser() //expose audio time and frequency data
  analyser.smoothingTimeConstant = 0.3
  analyser.fftSize = 256

  sourceNode = g_audioContext.createBufferSource() //to play audio data contained within an AudioBuffer object.
  sourceNode.connect(analyser)
  sourceNode.connect(g_audioContext.destination)
  analyser.connect(javascriptNode)
}

function playSound(buffer){
  sourceNode.buffer = buffer
  sourceNode.start(0)
    sourceNode.stop(1)
}
function stopSound(){
  sourceNode.stop()
}

function onError(e){
  console.log(e)
}
